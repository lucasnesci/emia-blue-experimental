## 📝  Description:
Some background information or a brief introduction if it is a new feature. Add as much information, links and images/screenshots as possible. Do not crop the screenshot, instead, print the full page (including the URL) and mark the part you want to highlight as this provides context.


## 🐾 Steps to reproduce
Specific steps to reproduce the issue.

_Example_:
1. Log in into the website @ https://www.emia.org/user/login
2. Create an Advocacy Plan @ https://www.emia.org/node/add/advocacy_plan
3. Fill all the fields
4.


## ❌ Current result
Why it is not working? Add screenshots.

_Example_: The plan is not saved. There is an error message saying the entity (taxonomy_term) cannot be referenced.


## ✅ Expected result
What should happen in order for the task to be successful?

_Example_: The plan is saved.


## 🔗 Other links / References


## 🖼️ Other images


## 🤖 Tech notes
No need to fill it.
